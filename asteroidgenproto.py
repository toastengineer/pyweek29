import math
import random

import pygame
import pymunk
import pymunk.pygame_util
import pymunk.autogeometry

def gen_asteroid_image(maxsize = 800):
    """Return a pygame Surface with a randomly generated asteroid on it."""
    if maxsize < 50: raise ValueError("maxsize must be >= 50")
    surf = pygame.Surface((random.randint(50, int(maxsize*4)), random.randint(50, int(maxsize*4))))
    rect = surf.get_rect() #type: pygame.Rect

    for t in range(random.randint(2, 4)):
        polar_vertices = []

        spikiness = random.uniform(0.0, 0.15)

        oldr = 0.5
        for t in range(random.randint(6, 24)):
            theta = random.random() * math.tau
            r = min(0.9, max(0.2, oldr + random.triangular(-1, 1, 0) * spikiness))
            oldr = r
            polar_vertices.append((r / 2, theta))

        polar_vertices.sort(key = lambda x: x[1])

        vertices = []
        for r, theta in polar_vertices:
            vertices.append((rect.width / 2 + round(r * rect.width * math.cos(theta)), rect.height / 2 + round(r * rect.height * math.sin(theta))))

        pygame.draw.polygon(surf, (255, 255, 255), vertices)
    return pygame.transform.smoothscale(surf, (rect.width // 4, rect.height // 4))

def surface_to_body(surf: pygame.Surface, space=None, density = 0.25):
    """Given a Pygame surface, return a pymunk Body that approximates it with Poly shapes,
    and add it to the given space if one is provided."""

    line_set = pymunk.autogeometry.PolylineSet()

    def sample_func(point):
        try:
            p = int(point.x), int(point.y)
            color = surf.get_at(p)
            return color.hsla[2]  # use lightness
        except IndexError:
            return 0

    pymunk.autogeometry.march_soft(
        pymunk.BB(0, 0, *surf.get_size()), surf.get_width(), surf.get_height(), 50,
        line_set.collect_segment,
        sample_func)

    if len(line_set) > 1:
        raise ValueError("Image seems to contain more than one polygon")
        #print(len(line_set))
    if len(line_set) == 0:
        raise ValueError("Couldn't find edges in image")

    hull_vertices = line_set[0]
    hull_vertices = pymunk.autogeometry.simplify_vertexes(hull_vertices, 1)

    body = pymunk.Body()
    if space is not None: space.add(body)

    polygons = pymunk.autogeometry.convex_decomposition(hull_vertices, 100)
    #polygons = [pymunk.autogeometry.to_convex_hull(hull_vertices, 1)]
    for vertices in polygons:
        #vertices = pymunk.autogeometry.simplify_curves(vertices, 0.5)
        shape = pymunk.Poly(body, vertices)
        shape.density = density
        shape.elasticity = 0.9
        if space is not None: space.add(shape)

    if body.mass == 0:
        space.remove(body, *body.shapes)
        raise ValueError("Failed?")

    return body

if __name__ == "__main__":
    screen = pygame.display.set_mode((800, 800))
    img_num = 0

    draw_options = pymunk.pygame_util.DrawOptions(screen)
    pymunk.pygame_util.positive_y_is_up = False
    space = pymunk.Space()

    while True:
        screen.fill((0,0,0))
        roid = gen_asteroid_image()
        roidrect = roid.get_rect()
        roidrect.center = screen.get_rect().center
        screen.blit(roid, roidrect)

        body = surface_to_body(roid, space)
        space.debug_draw(draw_options)
        space.remove(body, *body.shapes)

        pygame.display.flip()
        import time; time.sleep(5)
        #input()