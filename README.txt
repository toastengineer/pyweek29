Showdown 2881
Made for Pyweek 29 — "Butterfly Effect"

How to Play:
Install pygame and pymunk with PIP:
pip install -r requirements.txt

Run runme.py.

You are the green ship — it always points towards the mouse cursor.

Mouse1 to accelerate forward
Mouse2 to fire

The further the mouse is from the ship, the faster the shot will go.

Known Bugs:

Sometimes, when an enemy ship explodes, a dialog will pop up with an error message — just clicking
through it seems to work. It's an easy fix, but I ran out of time.

Tips:

Fired missiles arm half a second after they're fired — if a shot touches something before that, it'll just bounce
off.

If you're trapped against an asteroid by its gravity, try to spin away, rather than fighting gravity directly.

If you're trapped in the middle of a cluster of asteroids, get the whole thing spinning, so that it spins apart and
frees you.

Try to master the trick of firing a shot in the opposite direction you're moving so that the shot comes out stationary,
and it'll be pulled in by the target's gravity.