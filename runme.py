import itertools
import math
import os
import random
import sys
import time
from typing import *

import pygame
import pymunk
import pymunk.pygame_util

DESIRED_FRAMERATE = 30
PYHSICS_TICKS_PER_FRAME = 30
G = 667.3
RESOLUTION = 1600, 900
SCREENW, SCREENH = RESOLUTION
SCREEN_CENTER = pymunk.Vec2d(SCREENW//2, SCREENH//2)

ASTEROID_COLLISION = 1
MISSILE_COLLISION = 2
SHIP_COLLISION = 3
ENEMY_SHIP_COLLISION = 4

game_is_over = False
difficulty = 1

def main():
    space = pymunk.Space()
    space.gravity = (0, 0)

    pygame.init()
    screen = pygame.display.set_mode(RESOLUTION)
    clock = pygame.time.Clock()

    draw_options = pymunk.pygame_util.DrawOptions(screen)
    pymunk.pygame_util.positive_y_is_up = False

    space.add_collision_handler(MISSILE_COLLISION, ASTEROID_COLLISION).post_solve = on_missile_touch_something
    #space.add_collision_handler(MISSILE_COLLISION, MISSILE_COLLISION ).post_solve = on_missile_touch_something
    space.add_collision_handler(MISSILE_COLLISION, SHIP_COLLISION    ).post_solve = on_missile_touch_something
    space.add_collision_handler(SHIP_COLLISION, ASTEROID_COLLISION).post_solve = on_ship_crash
    space.add_collision_handler(MISSILE_COLLISION, ENEMY_SHIP_COLLISION).post_solve = on_missile_touch_something
    space.add_collision_handler(ENEMY_SHIP_COLLISION, ASTEROID_COLLISION).post_solve = on_enemy_ship_crash

    #sun = add_ball(space, position = (SCREENW // 2, SCREENH // 2))
    #sun.mass *= 150
    #sun.velocity = (0,0)

    for t in range(0):
        add_ball(space, random.expovariate(1 / 5))

    for t in range(30):
        add_asteroid(space)

    import shipproto
    global ship
    _, ship = shipproto.gen_spaceship(space)
    ship.position = random.randint(0, SCREENW), random.randint(0, SCREENH)
    ship.velocity_func = n_body_gravity
    ship.on_blown_up = lambda: game_over(space)
    for the_shape in ship.shapes:
        the_shape.collision_type = SHIP_COLLISION
        the_shape.color = (0, 255, 0)
    ship.last_bullet_time = 0

    reposition_coincident_objects(space)

    kill_plane = pymunk.BB(-200, -200, screen.get_width() + 200, screen.get_height() + 200)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT: pygame.quit(); sys.exit(0)
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE: pygame.quit(); sys.exit(0)

        global game_is_over
        if not game_is_over:
            update_player_ship(ship, space)

        #print(len([b for b in space.bodies if getattr(b, "is_enemy_ship", False)]))
        if len([b for b in space.bodies if getattr(b, "is_enemy_ship", False)]) < max(1, difficulty // 3):
            add_enemy_ship(space)

        update_enemy_ships(space, ship)

        for the_obj in space.bodies:
            the_obj:pymunk.Body
            if not kill_plane.contains_vect(the_obj.position) and \
                    the_obj.velocity.normalized().dot((SCREEN_CENTER - the_obj.position).normalized()) < 0:
                if the_obj is ship:
                    #Help the user get it back on screen
                    the_obj.velocity = (0, 0)
                else:
                    space.remove(the_obj, *the_obj.shapes)

                if getattr(the_obj, "is_asteroid", False):
                    add_asteroid(space, offscreen = True)

        screen.fill((0,0,0))
        tick_physics(space)
        #for theobj in space.bodies:
        #    pygame.draw.circle(screen, (255, 0, 0), tuple(map(round, theobj.position)), 4)
        space.debug_draw(draw_options)

        if game_is_over:
            draw_game_over_text(screen)

        pygame.display.flip()

        clock.tick(DESIRED_FRAMERATE)
        pygame.display.set_caption(f"Showdown 2881 {clock.get_fps()} FPS")

screen_radius = math.ceil(math.sqrt(SCREENW ** 2 + SCREENH ** 2))
def add_asteroid(space, offscreen = False):
    while True:
        try:
            import asteroidgenproto
            body = asteroidgenproto.surface_to_body(asteroidgenproto.gen_asteroid_image(100), space)
        except ValueError:
            continue

        body.is_asteroid = True
        if offscreen:
            come_in_from_edges(body)
        else:
            body.position = random.randint(0, SCREENW), random.randint(0, SCREENH)
            to_center = (pymunk.Vec2d(SCREENW // 2, SCREENH // 2) - body.position)
            dist = to_center.length
            body.velocity = (pymunk.Vec2d(SCREENW // 2, SCREENH // 2) - body.position).perpendicular_normal() * 8000 / max(dist, 1)
        body.velocity += (random.randint(-50, 50), random.randint(-50, 50))
        body.velocity_func = n_body_gravity
        for the_shape in body.shapes:
            the_shape.collision_type = ASTEROID_COLLISION

        return body


def come_in_from_edges(body):
    r = random.randint(screen_radius, screen_radius + 100)
    theta = random.random() * math.tau
    body.position = r * math.cos(theta), r * math.sin(theta)
    to_center = (pymunk.Vec2d(SCREENW // 2, SCREENH // 2) - body.position)
    body.velocity = to_center.normalized() * random.triangular(50, 100 * difficulty, 50)


def tick_physics(space):
    for t in range(2):
        calculate_gravity(space)
        for t2 in range(15):
            space.step(1 / DESIRED_FRAMERATE / 30)


def explosion(space, origin, power = 10000000):
    radius = 0.0000015 * power
    for the_obj in space.bodies:
        the_obj: pymunk.Body
        here_to_there = (the_obj.local_to_world(the_obj.center_of_gravity) - origin)
        distance = here_to_there.normalize_return_length()
        if 0.01 < distance < radius:
            the_obj.apply_impulse_at_world_point(here_to_there * (power / distance**2), origin)
            if hasattr(the_obj, "on_blown_up"): the_obj.on_blown_up()
        pygame.draw.circle(pygame.display.get_surface(), (255,0,0), tuple(map(int,origin)), int(radius))

def update_player_ship(ship, space):
    now = time.monotonic()

    mouse_pos = pygame.mouse.get_pos()
    force_direction = (ship.world_to_local(mouse_pos)).normalized()

    ship.apply_force_at_local_point(force_direction * 1000000, (0, -10))
    if not pygame.mouse.get_pressed()[0]:
        ship.apply_force_at_local_point(force_direction * -1000000, (0, 10))

    if pygame.mouse.get_pressed()[1]:
        explosion(space, mouse_pos)

    if pygame.mouse.get_pressed()[2] and now - ship.last_bullet_time > 0.25:
        bullet = add_missile(space, ship.local_to_world((0, -12)), ship.velocity)
        bullet.velocity += (mouse_pos - bullet.position) * 1.2
        ship.last_bullet_time = now

    ship.angular_velocity *= 0.01

def update_enemy_ships(space, player):
    now = time.monotonic()
    for ship in [b for b in space.bodies if getattr(b, "is_enemy_ship", False)]:
        force_direction = ship.world_to_local(player.position).normalized()
        to_player = player.position - ship.position
        distance = to_player.normalize_return_length()
        if ship.velocity.dot(to_player) < max(distance, 50):
            ship.apply_force_at_local_point(force_direction * 1000000, (0, -10))
        if ship.velocity.dot(to_player) > max(distance - 10, 50):
            ship.apply_force_at_local_point(force_direction * -1000000, (0, 10))
        if force_direction.dot((0,-1)) > 0.99 and distance < 700 and now - ship.last_bullet_time > 0.5:
            bullet = add_missile(space, ship.local_to_world((0, -12)), ship.velocity)
            bullet.velocity += (player.position - bullet.position).normalized() * 200
            ship.last_bullet_time = now
        ship.angular_velocity *= 0.1
        ship.velocity *= 0.99

def reposition_coincident_objects(space):
    while True:
        try:
            # If any objects are on top of each other we'll get a ZeroDivisionError
            calculate_gravity(space)
        except ZeroDivisionError:
            for a in space.bodies:
                for b in space.bodies:
                    if a is b: continue
                    a_to_b: pymunk.Vec2d = b.local_to_world(b.center_of_gravity) - a.local_to_world(a.center_of_gravity)
                    if a_to_b.length < 10:
                        b.position = random.randint(0, SCREENW), random.randint(0, SCREENH)
                        print("Repositioning object...")
                        continue
        else:
            break


def n_body_gravity(body: pymunk.body, _, damping, dt):
    body.update_velocity(body, getattr(body, "gravity", (0,0)), damping, dt)

def calculate_gravity(the_space: pymunk.Space):
    for theobj in the_space.bodies: theobj.gravity = pymunk.Vec2d()

    for a, b in itertools.combinations(the_space.bodies, 2):
        a: pymunk.Body
        b:pymunk.Body
        a_to_b: pymunk.Vec2d = b.local_to_world(b.center_of_gravity) - a.local_to_world(a.center_of_gravity)
        magnitude = (G * a.mass * b.mass) / ((a_to_b.normalize_return_length()) ** 2)
        a.gravity += a_to_b * magnitude
        b.gravity -= a_to_b * magnitude
        #a.force += a_to_b * magnitude
        #b.force += a_to_b * -magnitude

    for theobj in the_space.bodies: theobj.gravity /= theobj.mass

def add_ball(space, radius = 25, position = None):
    if position is None: position = random.randint(0, SCREENW), random.randint(0, SCREENH)
    inertia = pymunk.moment_for_circle((radius * 10) ** 3, 0, radius)
    body = pymunk.Body(radius, inertia)
    body.velocity_func = n_body_gravity
    shape = pymunk.Circle(body, radius)#pymunk.Poly(body, [(-10, -10), (10, 10), (10, -10), (-10, 10)])
    shape.elasticity = 0.25
    body.position = position
    dist = (pymunk.Vec2d(SCREENW // 2, SCREENH // 2) - position).length
    body.velocity = (pymunk.Vec2d(SCREENW//2, SCREENH//2) - position).perpendicular_normal() * 20000 / max(dist, 1)
    space.add(body, shape)
    return body

def add_missile(space, position, velocity):
    body = pymunk.Body(25, pymunk.inf)
    body.velocity_func = n_body_gravity

    shape = pymunk.Circle(body, 3)

    body.position = position
    body.velocity = velocity

    shape.collision_type = MISSILE_COLLISION

    space.add(body, shape)
    body.is_missile = True
    body.time_of_creation = time.monotonic()
    def on_blown_up():
        del body.on_blown_up
        space.remove(body, *body.shapes)
        explosion(space, body.position)
    body.on_blown_up = on_blown_up

    return body

def on_missile_touch_something(arbiter, space, data):
    bodies_involved = set(s.body for s in arbiter.shapes)
    now = time.monotonic()
    if arbiter.is_first_contact:
        for the_obj in bodies_involved:
            if getattr(the_obj, "is_missile", False):
                if now - the_obj.time_of_creation > 0.5:
                    explosion(space, the_obj.position)
                    space.remove(the_obj, *the_obj.shapes)

def on_ship_crash(arbiter, space, data):
    if arbiter.total_ke > 2_500_000:
        game_over(space)

def on_enemy_ship_crash(arbiter, space, data):
    enemy_ship = next(b for b in {s.body for s in arbiter.shapes} if getattr(b, "is_enemy_ship", False))
    if arbiter.total_ke > 1_000_000:
        on_enemy_killed(space, enemy_ship)

def game_over(space):
    global game_is_over
    game_was_over = game_is_over #prevent infinite recursion when ship's own explosion blows it up
    game_is_over = True
    if not game_was_over:
        explosion(space, ship.position, 500000000)

def draw_game_over_text(screen):
    font = pygame.font.Font("FSEX300.ttf", 16)
    text = font.render("G A  M E  O V E R", 1, (255,127,127))
    screen.blit(text, SCREEN_CENTER)

def add_enemy_ship(space):
    import shipproto
    _, ship = shipproto.gen_spaceship(space)
    ship.velocity_func = n_body_gravity
    ship.on_blown_up = lambda: on_enemy_killed(space, ship)
    for the_shape in ship.shapes:
        the_shape.collision_type = ENEMY_SHIP_COLLISION
        the_shape.color = (200, 50, 50)
    come_in_from_edges(ship)
    ship.is_enemy_ship = True
    ship.last_bullet_time = 0
    return ship

def on_enemy_killed(space, enemy):
    del enemy.on_blown_up
    space.remove(enemy, *enemy.shapes)
    explosion(space, enemy.position, 15000000)
    add_enemy_ship(space)
    global difficulty
    difficulty += 1

if __name__ == "__main__":
    main()