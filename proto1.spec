# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['proto1.py'],
             pathex=['C:\\Users\\Schilcote\\PycharmProjects\\pyweek29'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='Showdown 2881',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=False )
import os, pymunk
pymunk_dir = os.path.dirname(pymunk.__file__)
chipmunk_libs = [
    ('chipmunk.dll', os.path.join(pymunk_dir, 'chipmunk.dll'), 'DATA'),
]
font = [
    ('FSEX300.ttf', 'FSEX300.ttf', 'DATA')
]
coll = COLLECT(exe,
               a.binaries + chipmunk_libs + font,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='proto1')
