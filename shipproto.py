import pygame
import pymunk
import pymunk.pygame_util

spaceship_vertices = ((25, 0), (50, 50), (25, 30), (0, 50))
def gen_spaceship(space = None):
    """Return a tuple of (surf, body); surf is a Pygame surface with the image, body is a Pymunk body that
    represents it in the physics model. If space is provided, adds the ship to the space."""
    surf = pygame.Surface((1000, 1000))
    pygame.draw.polygon(surf, (255,255,255), [(x*10, y*10) for x, y in spaceship_vertices])
    surf = pygame.transform.smoothscale(surf, (100, 100))

    left_half_vertices  = ((5, 0), (5, 6), ( 0, 10))
    right_half_vertices = ((5, 0), (10, 10), (5, 6))

    body = pymunk.Body()
    left_half  = pymunk.Poly(body, left_half_vertices,  pymunk.Transform(tx=-5, ty=-5))
    right_half = pymunk.Poly(body, right_half_vertices, pymunk.Transform(tx=-5, ty=-5))

    left_half.density = right_half.density = 5

    if space is not None: space.add(body, left_half, right_half)

    return surf, body

if __name__ == "__main__":
    screen = pygame.display.set_mode((800, 800))
    clock = pygame.time.Clock()

    draw_options = pymunk.pygame_util.DrawOptions(screen)
    pymunk.pygame_util.positive_y_is_up = False
    space = pymunk.Space()

    ship, body = gen_spaceship(space)
    #body.velocity = (10, 10)

    theta = 0
    while True:
        screen.fill((0,0,0))

        pygame.event.pump()
        mouse_pos = pygame.mouse.get_pos()
        force_direction = (body.world_to_local(mouse_pos) - (25, 0)).normalized()
        body.apply_force_at_local_point(force_direction * 1000000, (25, 0))
        #body.apply_force_at_world_point()
        space.step(1/60)

        space.debug_draw(draw_options)

        pygame.display.flip()
        clock.tick(60)
